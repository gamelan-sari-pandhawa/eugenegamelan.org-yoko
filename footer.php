<?php
/**
 * @package WordPress
 * @subpackage Yoko
 */
?>

</div><!-- end wrap -->

	<footer id="colophon" class="clearfix">
		<p></p>
		<a href="#page" class="top"><?php _e( 'Top', 'yoko' ); ?></a>
	</footer><!-- end colophon -->

</div><!-- end page -->
<?php wp_footer(); ?>

</body>
</html>
