<?php
function child_enqueue_styles() {
	$parent_name = 'yoko';
	$child_name  = 'yoko-sari-pandhawa';

	wp_enqueue_style(
		$parent_name,
		get_template_directory_uri() . '/style.css'
	);

	wp_enqueue_style(
		$child_name,
		get_stylesheet_directory_uri() . '/style.css',
		array( $parent_name )
	);

}
add_action( 'wp_enqueue_scripts', 'child_enqueue_styles' );
?>
